<?php

namespace App\Providers;

use App\event;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        function boot()
        {
            Schema::defaultStringLength(191); //NEW: Increase StringLength
        }
//        View::composer('*',function($view){
//            $latest_events = $latest_events = Event::orderBy('created_at','desc')->limit(2)->get();
//            $view->with('latest_events', $latest_events);
//        });
    }
}
