<?php

namespace App\Http\Controllers;

use App\event;
use App\event_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param $type event_type
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = DB::table('events')->paginate(9);

        return view('events')->with('events', $events);
    }

    public function by_type(Request $request)
    {
        $path=$request->path();
        $events=[];
        switch ($path){
            case "exhibition":
                array_push($events , event::where('category', 'cafe')->paginate(6));
                array_push($events , event::where('category', 'industry')->paginate(6));
                array_push($events , event::where('category', 'media')->paginate(6));
                array_push($events , event::where('category', 'startup')->paginate(6));
                array_push($events , event::where('category', 'decoration')->paginate(6));
                array_push($events , event::where('category', 'tea_coffee_sweets')->paginate(6));
                array_push($events , event::where('category', 'pro_courses')->paginate(6));
                return view($path)->with('events',$events);
                break;
            case "side_events":
                array_push($events , event::where('category', 'concert')->paginate(6));
                array_push($events , event::where('category', 'folks_festival')->paginate(6));
                array_push($events , event::where('category', 'theater')->paginate(6));
                break;
            case "contest":
                array_push($events , event::where('category', 'good_cafe')->paginate(6));
                array_push($events , event::where('category', 'cafe_photography')->paginate(6));
                array_push($events , event::where('category', 'portable')->paginate(6));
                array_push($events , event::where('category', 'hosting')->paginate(6));
                array_push($events , event::where('category', 'cooking')->paginate(6));
                array_push($events , event::where('category', 'cold_bar')->paginate(6));
                return view($path)->with('events',$events);
                break;
            case "courses":
                array_push($events , event::where('category', 'hosting_course')->paginate(6));
                array_push($events , event::where('category', 'cooking_course')->paginate(6));
                array_push($events , event::where('category', 'cold_bar_course')->paginate(6));
                return view($path)->with('events',$events);
                break;
            case "news":
                $events=event::where('category', 'news')->paginate(6);
                return view($path)->with('events',$events);
                break;
        }
        return view($request->path())->with('events',$events);
    }

    public function by_cat($cat){
        $events = event::where('category',$cat)->paginate(50);
        return view('dashboard/events')->with('events',$events)->with('cat',$cat);
    }


    public static function latest($number = 6)
    {
        $latest_events = Event::orderBy('created_at', 'desc')->limit($number)->get();
        return view('index')->with('latest_events', $latest_events);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param $cat
     * @return \Illuminate\Http\Response
     */
    public function create($cat)
    {

        return view('dashboard/new')->with('cat',$cat);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $cat
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$cat)
    {
//        $request->validate([
//            'title'=>'required',
//            'description'=>'required',
//            'type'=>'required',
//            'category'=>'required',
//        ]);
//        dd($request->hasFile('image'));
        $path = $request->file('image')->store('public');
        $path = str_replace("public/","storage/",$path);
        $request = $request->all();
        $request['created_by'] = Auth::user()->id;
        $request['image']=$path;
        $request['type'] = "asd";
        $request['category'] = $cat;
        //$request['event_id'] = $id[count($id)-1];

        //dd($path);
        Event::create($request);

        return redirect()->route('new',$cat);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::findOrFail($id);
        $events = DB::table('events')->where('category','news')->
        orderBy('created_at','desc')->limit(3)->get();
        return view('event')->with('event', $event)->with('events',$events);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function edit(event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\event $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(event $event)
    {
        //
    }
}
