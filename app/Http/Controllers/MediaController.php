<?php

namespace App\Http\Controllers;

use App\media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medias=[];
        array_push($medias ,media::where('category','first')->paginate(8));
        array_push($medias ,media::where('category','second')->paginate(8));
        array_push($medias ,media::where('category','third')->paginate(8));
        return view('gallery')->with('medias',$medias);
    }

    public function by_cat($cat){
        $medias = media::where('category',$cat)->paginate(5);
        return view('dashboard/gallery')->with('medias',$medias)->with('cat',$cat);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cat)
    {
        return view('dashboard/newMedia')->with('cat',$cat);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$cat)
    {
        $path = $request->file('media')->store('public');
        $path = str_replace("public/","storage/",$path);
        $request = $request->all();
        $request['name']=$path;
        $request['category'] = $cat;

        media::create($request);

        $medias = media::where('category',$cat)->paginate(10);
        return view('dashboard/gallery')->with('medias',$medias)->with('cat',$cat);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit(media $media)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, media $media)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res= media::where('id',$id);
        $cat = $res->get()[0]->cat;
        $res->delete();
        return ($this->by_cat($cat));
    }
}
