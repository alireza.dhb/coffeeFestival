<?php

namespace App\Http\Controllers;

use App\Enroll;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use GuzzleHttp;

class EnrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enrolls = DB::table('enrolls')->orderBy('created_at','desc')->paginate(10);
        return view('dashboard/registration')->with('enrolls', $enrolls);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $request->validate([
//            'type'=>'required',
//            'sub_type'=>'required',
//            'name'=>'required',
//            'agent_name'=>'required',
//            'address'=>'required'
//        ]);
//         dd($request->all());
        $request = $request->all();
        if ($request['unit_name'] != null) {
            $request['name'] = $request['unit_name'];
            $request['address'] = $request['address_2'];
            $request['manager_name'] = $request['manager_name_2'];
            $request['phone'] = $request['phone_2'];
        } else {
            $request['name'] = $request['cafe_name'];
            $request['address'] = $request['address_1'];
            $request['manager_name'] = $request['manager_name_1'];
            $request['phone'] = $request['phone_1'];

        }
//        dd($request);
        $enroll = Enroll::create($request);
        $reservedBooths = DB::table('enrolls')->select('booth')->whereNotNull('booth')->whereNotNull('payment_id')->get();

        $client = new GuzzleHttp\Client();
//        dd($enroll);
        if ($enroll->type == '30') {
            return view("mapInfo")->with('enroll', $enroll);
        }
        return view("mapSelection")->with('id', $enroll->id)->with('reservedBooths', $reservedBooths);
    }

    public function selectMap(Request $request)
    {
        $request = $request->all();
        DB::table('enrolls')->where('id', $request['id'])->update(['booth' => $request['booth']]);
        $enroll = DB::table('enrolls')->where('id', $request['id'])->first();
        return view('mapInfo')->with('enroll', $enroll);
    }

    public function confirm(Request $request)
    {
        $enroll = DB::table('enrolls')->where('id', $request['id'])->first();
        $client = new GuzzleHttp\Client();
        if ($enroll->phone != null && preg_match('/(0)[0-9]{10}/',$enroll->phone)) {
            if(DB::table('receptors')->where('phone',$enroll->phone)->count()==0){
                DB::table('receptors')->insert([['phone'=>$enroll->phone]]);

            }
            $res = $client->get('https://api.kavenegar.com/v1/33476D6852647047466A4B747A4A5A39326C564F326A77566845642F6D554C2F/sms/send.json?' .
                'receptor=' . $enroll->phone . '&sender=10004346&message=' . "میزبانی از شما در جشنواره و نمایشگاه کافه مقصد گردشگری شهری باعث افتخار ماست. به جمع ما خوش آمدید.");

        }

        return redirect('');
    }
    public function adminConfirm(Request $request)
    {
        $enroll = DB::table('enrolls')->where('id', $request['id'])->update(['payment_id'=>'0']);

        return redirect('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enroll $enroll
     * @return \Illuminate\Http\Response
     */
    public function show(Enroll $enroll)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enroll $enroll
     * @return \Illuminate\Http\Response
     */
    public function edit(Enroll $enroll)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Enroll $enroll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('enrolls')->where('id',$request['id'])
            ->update(['name'=>$request['name'], 'manager_name'=>$request['manager_name'],
            'phone'=>$request['phone'],'booth'=>$request['booth']]);
        return redirect('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enroll $enroll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DB::table('enrolls')->where('id',$request['id'])->delete();
        return redirect('/admin');
    }
}
