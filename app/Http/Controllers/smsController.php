<?php
/**
 * Created by PhpStorm.
 * User: alirezadhb
 * Date: 7/27/19
 * Time: 2:34 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class smsController extends Controller
{
    public function show(){
        return view('dashboard/sms');
    }

    public function send(Request $request){
        $data = $request->all();
        //create guzzle client
        $client = new GuzzleHttp\Client();
        // convert receptors collection to string
        $count = DB::table('receptors')->count();
        for ($i = 0; $i < round($count/200)+1; $i++){
            $receptors=DB::table('receptors')->skip($i*200)->take(($i+1)*200)->get();
            $res = $client->get('https://api.kavenegar.com/v1/33476D6852647047466A4B747A4A5A39326C564F326A77566845642F6D554C2F/sms/send.json?'.
                'receptor='.$receptors->implode('phone',',').'&sender=10004346&message='. $data['content']);
            echo ($i*200).' '.(($i+1)*200);
        }
        return view('dashboard/sms')->with('status',$res->getStatusCode());

    }
}