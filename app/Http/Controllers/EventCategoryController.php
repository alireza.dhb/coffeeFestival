<?php

namespace App\Http\Controllers;

use App\event_category;
use Illuminate\Http\Request;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\event_category  $event_category
     * @return \Illuminate\Http\Response
     */
    public function show(event_category $event_category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\event_category  $event_category
     * @return \Illuminate\Http\Response
     */
    public function edit(event_category $event_category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\event_category  $event_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, event_category $event_category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\event_category  $event_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(event_category $event_category)
    {
        //
    }
}
