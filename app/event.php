<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class event extends Model
{
    protected $fillable = array('title', 'description', 'content','image', 'type', 'category', 'created_by');

    public function medias()
    {
        return $this->hasMany('App\Media');
    }
}
