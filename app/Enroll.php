<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enroll extends Model
{
    protected $fillable = array('type','sub_type','name','manager_name',
        'address','email','instagram','telegram','agent_name','agent_post','phone','activity_field');

    public function event(){
        return $this->belongsTo('App\event');
    }
}
