<html>
    <head>
        @include('head')
    </head>
    <body>
		@include('header')
		
		 @include('index')
		{{-- @include('gallery') --}}
		@include('contest')
		{{-- @include('event') --}}
		{{-- @include('showplace') --}}
		{{-- @include('aboutus') --}}
		{{-- @include('contactus') --}}
		{{-- @include('single') --}}

        @include('footer')
        @include('script')
    </body>
</html>