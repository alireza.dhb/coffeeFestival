  	<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
		      <a  class="navbar-brand font-force" href="{{ url('') }}">
				  <img style="height: 100px;" src="{{url("images/gallery/logoOrginal.png")}}" alt="">
			  </a>
		      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
		        <span class="oi oi-menu"></span> منو
		      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
	            <li class="nav-item {{request()->is('/') ? 'active' : '' }}"><a href="{{ url('') }}" class="nav-link">صفحه اصلی</a></li>
				<li class="nav-item {{request()->is('exhibition') ? 'active' : '' }}"><a href="{{ url('exhibition') }}" class="nav-link">نمایشگاه</a></li>
				<li class="nav-item {{request()->is('side_events') ? 'active' : '' }}"><a href="{{ url('side_events') }}" class="nav-link">رویداد های جانبی</a></li>
				<li class="nav-item {{request()->is('contest') ? 'active' : '' }}"><a href="{{ url('contest') }}" class="nav-link">مسابقات</a></li>
	            <li class="nav-item {{request()->is('courses') ? 'active' : '' }}"><a href="{{ url('courses') }}" class="nav-link">دوره های آموزشی</a></li>
				<li class="nav-item {{request()->is('gallery') ? 'active' : '' }}"><a href="{{ url('gallery') }}" class="nav-link">گالری</a></li>
				<li class="nav-item {{request()->is('video') ? 'active' : '' }}"><a href="{{ url('video') }}" class="nav-link">تیزر ها</a></li>
				<li class="nav-item {{request()->is('news') ? 'active' : '' }}"><a href="{{ url('news') }}" class="nav-link">اخبار</a></li>
				<li class="nav-item {{request()->is('about') ? 'active' : '' }}"><a href="{{ url('about') }}" class="nav-link">درباره ی ما</a></li>
				{{--<li class="nav-item {{ request()->is('contact') ? 'active' : '' }}"><a href="{{ url('contact') }}" class="nav-link">ارتباط با ما</a></li>--}}
	        </ul>
	      </div>
		  </div>
	  </nav>