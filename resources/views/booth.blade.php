<html>
<head>
    @include('head')
</head>
<body>
@include('header')
<section class="ftco-intro mt-5">
    <div style="border: 1px solid #9090907a; border-radius: 25px;" class="container mb-5">
        <form action="{{url('booth')}}" method="post">
        @csrf
        <div class="row mt-3 mb-2">
            <div class="col-md-12 text-center font-force">
                <h2 class="font-force-title mt-5 mb-4">فرم درخواست حضور در سومین جشنواره و نمايشگاه
                    کافه مقصد گردشگری شهری
                </h2>
            <h5 class="font-force mb-4">خیابان ولیعصر، خیابان شهید فلاحی ( زعفرانیه ) انتهای خیابان شهید کمال طاهری
                مجموعه فرهنگی تاریخی سعدآباد
            </h5>
                <p class="mb-5">تاریخ : 3/6/1398 لغایت 8/6/1398
                    ساعت  16 الی 22
                </p>
            </div>
            <div class="col-md-12 text-center font-force">
                <h2 class="font-force-title mt-5 mb-4 text-right">گروه نمایشگاه</h2>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group text-right">
                            <select name="type" class="form-control" id="coffeOrSanaye">
                                <option value="20">کافه</option>
                                <option value="10">صنایع وابسته</option>
                                <option value="30">مسابقات</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group text-right">
                            {{-- در صورتی که صنایع وابسته انتخاب شد در دراپ قبلی--}}
                            <select name="sub_type" class="form-control" id="dropCoffeOrSanaye">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center font-force">

                <h2 class="font-force-title mt-5 mb-4 text-right">انتخاب نوع شخصیت </h2>
                <p class="text-right">فقط اطلاعات یکی از شخصیت ها پر شود</p>
                <div class="accordion" id="accordionExample">
                    <div class="card-edited">
                        <div class="card-header  text-right" id="headingOne">
                            <h2 class="mb-0">
                                <button id="btn-hoghoghi" class="btn btn-primary font-force" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    شخصیت حقوقی
                                    <i class="fas fa-chevron-down"></i>
                                </button>
                            </h2>
                        </div>
                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">نام شرکت / کافه : </label>
                                            <input name="cafe_name" type="text" class="form-control"  id="name-hoghoghi" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="manager_name">نام و نام خانوادگی مدیرعامل:</label>
                                            <input id="family-hoghoghi" name="manager_name_1" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">تلفن همراه:</label>
                                            <input id="phone-hoghoghi" name="phone_1" type="tel" class="form-control"  >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for=""> نشانی و تلفن شرکت/ کافه:</label>
                                            <input name="address_1" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-edited">
                        <div class="card-header text-right" id="headingTwo">
                            <h2 class="mb-0">
                                <button id="btn-haghighi" class="btn btn-primary  font-force collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    شخصیت حقیقی
                                    <i class="fas fa-chevron-down"></i>
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">نام واحد : </label>
                                            <input name="unit_name" type="text" class="form-control"  id="name-haghighi" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">نام و نام خانوادگی مسئول:</label>
                                            <input name="manager_name_2" type="text" class="form-control" id="family-haghighi" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for=""> نشانی و تلفن:</label>
                                            <input name="address_2" type="tel" class="form-control"  id="phone-haghighi" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for=""> پست الکترونیک:</label>
                                            <input name="email" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">آدرس اینستاگرام:</label>
                                            <input name="instagram" type="text" class="form-control" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">آدرس تلگرام:</label>
                                            <input name="telegram" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">نام و نام خانوادگی  نماینده معرفی شده:</label>
                                            <input name="agent_name" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for=""> سمت نماینده معرفی شده:</label>
                                            <input name="agent_post" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">تلفن همراه:</label>
                                            <input name="phone_2" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="form-group">
                                            <label class="text-right text-light" for="">موضوع فعالیت:</label>
                                            <input name="activity_field" type="text" class="form-control" id="" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center font-force">
                <div class="collapse" id="collapseExample1">
                    <p class="text-right">-
                        ثبت نام قطعي، پس با اولیت از تكميل فرم درخواست مشاركت و پرداخت كامل هزينه هاي مربوطه از طریق سامانه جشنواره به نشانی cafefestival.ir بخش ثبت نام جشنواره و حداکثر تا تاریخ 20/05/98
                        انجام مي گردد.(این تاریخ غیر قابل تمدید است)
                        <br>
                         پس از تکمیل ثبت نام امکان انصراف و استرداد وجه وجود نخواهد داشت .
                        <br>
                         شرکت کنندگان بخش نمایشگاهی می بایست طرح غرفه خود را تا تاریخ 20/05/98 برای تأیید به سامانه(cafefestival.ir) جشنواره ارسال نمایند.
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                <h2 class="font-force-title mt-5 mb-4 text-right">  شرایط </h2>
                <p class="text-right">
                    ثبت نام قطعي، پس با اولیت از تكميل فرم درخواست مشاركت و پرداخت كامل هزينه هاي مربوطه از طریق سامانه جشنواره به نشانی cafefestival.ir بخش ثبت نام جشنواره و حداکثر تا تاریخ 20/05/98
                    انجام مي گردد.(این تاریخ غیر قابل تمدید است)
                    - پس از تکمیل ثبت نام امکان انصراف و استرداد وجه وجود نخواهد داشت .
                    <br>
                    - شرکت کنندگان بخش نمایشگاهی می بایست طرح غرفه خود را تا تاریخ 20/05/98 برای تأیید به سامانه(cafefestival.ir) جشنواره ارسال نمایند.

                </p>
            </div>
            <div class="col-md-12 text-center font-force">
                <h2 class="font-force-title mt-5 mb-4 text-right">مقررات عمومی نمایشگاه</h2>
                <p class="text-right">
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        مشاهده قوانین
                        <i class="fas fa-chevron-down"></i>
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <p class="text-right">
                        1-	شرکت کنندگان موظف به حفظ و نگهداری فضای مجموعه می باشند، نصب و پیچ کردن هر وسیله ای که باعث تخریب و آسیب به دیواره ها و فضای آن گردد ممنوع و شامل جریمه خواهد بود.
                        <br>
                        2-	شرکت کنندگان مجاز به واگذاری غرفه (تمام و یا بخشی) به غیر و تعطیل نمودن غرفه در ساعت بازدید نمی باشند.
                        <br>
                        3-	رعایت شئونات اسلامی ( اعم از موسیقی، پوشش، تصاویر و ...) در چهارچوب قوانین رسمی جمهوری اسلامی ایران، برای غرفه داران الزامی است.
                        <br>
                        4-	نصب کارت شناسایی غرفه دار در طول ساعات کاری نمایشگاه الـزامی بوده و از ورود غرفه داران بدون کارت شناسایی در نمایشگاه جلوگیری می شود.
                        <br>
                        5-	کلیه غـرفه داران می بـایست 1 ساعـت قـبل از شـروع ساعت کـار رسمی نمایشگـاه در محـل غـرفه خـود حاضـر باشنـد، در غیر اینصورت ستاد اجرائی نمایشگاه هیچگـونـه مسئولیتی در قـبال اجناس آنان ندارد. (ساعـت کار رسمی  نمایشگـاه 14  الی 22 می باشد.)
                        <br>
                        6-	همراه داشتن چراغ شارژی ( اضطراری) و کپسول آتش نشانی الزامی می باشد.
                        <br>
                        7-	استفاده از وسایل گرمایشی، سرمایشی و پرژکتورهای پر مصرف اکـیداً ممنوع می باشد و در صورت مشاهـده با غـرفـه متخلف برخورد قانونی و نسبت به تعطیل نمودن آن اقدام خواهد شد.
                        <br>
                        8-	در صورت لازم بودن استفاده از یخچال جهت نگهداری مواد غذائی، سرخ کن برقی، دستگاه رست قهوه و مایکرویو و سایر تجهیزات مورد نیاز برقی مراتب را کـتباً همزمان با ثبت نام به ستاد اجرائی نمایشگاه اعلام فرمائید.
                    </p>
                </div>
                <div class="form-group form-check text-right ">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                    <label class="form-check-label mr-4 text-light" for="exampleCheck1">اینجانب  با اطلاع و پذیرش کامل مقررات و شرایط عمومی نمایشگاه نسبت به عقد قرارداد اقدام می نمایم  </label>
                </div>
                <div class="text-right mt-2 mb-2">
                <button style="background: #2dc14d;border: 1px solid #43b31f;color: #fff;" type="submit" class="btn btn-primary text-right">ثبت درخواست</button>
                </div>
            </div>
        </div>
        </form>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>