<html>
<head>
    @include('head')
</head>
<body>
@include('header')

<section class="ftco-menu">
    <div class="container-fluid">
        <div class="row d-md-flex">
            <div class="col-lg-12 ftco-animate p-md-5">
                <div class="row">
                    <div class="col-md-6 pb-4">
                        <h3 class="text-center font-force-title">اولین دوره</h3>
                        <div class="w-100">
                            <video  class="js-player" poster="/path/to/poster.jpg" id="player" playsinline >
                                <source src="videos/AvalinDooreh.mp4" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                    <div class="col-md-6  pb-4">
                        <h3 class="text-center font-force-title"> دوره آموزشی آشپزی درکافه</h3>
                        <div class="w-100">
                            <video  class="js-player" poster="/path/to/poster.jpg" id="player" playsinline >
                                <source src="videos/dooreAmoozeshiAshpaziDarKhane.mp4" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                    <div class="col-md-6  pb-4">
                        <h3 class="text-center font-force-title"> دوره آموزشی میزبانی درکافه</h3>
                        <div class="w-100">
                            <video  class="js-player" poster="/path/to/poster.jpg" id="player" playsinline >
                                <source src="videos/MizbaniDarCoffe.mp4" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                    <div class="col-md-6  pb-4">
                        <h3 class="text-center font-force-title"> تیزر کافه خوب</h3>
                        <div class="w-100">
                            <video  class="js-player" poster="/path/to/poster.jpg" id="player" playsinline >
                                <source src="videos/TizerCoffeKhob.mp4" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                    <div class="col-md-6  pb-4">
                        <h3 class="text-center font-force-title">دومین دوره</h3>
                        <div class="w-100">

                            <video  class="js-player" poster="/path/to/poster.jpg" id="player" playsinline >
                                <source src="videos/DovominDooreh.mp4" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                    <div class="col-md-6  pb-4">
                        <h3 class="text-center font-force-title">سومین دوره</h3>
                        <div class="w-100">

                            <video  class="js-player" poster="/path/to/poster.jpg" id="player" playsinline >
                                <source src="videos/sevominDooreh.mp4" type="video/mp4" />
                            </video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>