<html>
<head>
    @include('head')
</head>
<body>
@include('header')
<section class="ftco-section ftco-degree-bg text-right">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ftco-animate">
                <p>
                    <img src="{{ asset($event->image) }}" alt="" class="img-fluid">
                </p>
                <h2 class="mb-3 font-force">{{$event->title}}</h2>
                <p>{{$event->description}}</p>
                {!! $event->content !!}


            </div> <!-- .col-md-8 -->
            <div class="col-md-4 sidebar ftco-animate">
                <div class="sidebar-box ftco-animate">
                    <h3 class="font-force">آخرین اخبار</h3>

                    @foreach($events as $news)
                    <div class="block-21 mb-4 d-flex">
                        <a class="blog-img ml-4" style="background-image: url({{asset($news->image)}});"></a>
                        <div class="text font-force">
                            <h3 class="heading font-force"><a href="{{url("event/".$news->id)}}">{{$news->title}}</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="icon-calendar"></span>{{$news->created_at}}</a></div>

                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>


            </div>

        </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>