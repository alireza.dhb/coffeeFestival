<html>
<head>
    @include('head')
</head>
<body>
@include('header')
<section class="ftco-menu">
    <div class="container-fluid">
        <div class="row d-md-flex">
            <div class="col-lg-12 ftco-animate p-md-5">
                <div class="row">
                    <div class="col-md-12 nav-link-wrap mb-5">
                        <div class="nav ftco-animate nav-pills" id="v-pills-tab" role="tablist"
                             aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1"
                               role="tab" aria-controls="v-pills-1" aria-selected="true"> اولین دوره</a>
                            <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab"
                               aria-controls="v-pills-2" aria-selected="false">دومین دوره</a>
                            <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab"
                               aria-controls="v-pills-3" aria-selected="false">سومین دوره</a>
                        </div>
                    </div>
                    <div class="col-md-12 d-flex align-items-center">
                        <div class="tab-content ftco-animate w-100" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel"
                                 aria-labelledby="v-pills-1-tab">
                                <section class="ftco-gallery pb-5 pt-5">
                                        <div class="row no-gutters">
                                            @foreach($medias[0] as $media)
                                            <div class="col-md-3 ftco-animate">
                                                <a href="{{asset($media->name)}}" class="gallery img d-flex align-items-center group"
                                                   style="background-image: url({{asset($media->name)}});">
                                                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                                                        <span class="icon-search"></span>
                                                    </div>
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    <div class="row mt-5">
                                        <div class="col text-center">
                                            <div id="pagination" class="block-27">
                                                {{ $medias[0]->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane fade" id="v-pills-2" role="tabpanel"
                                 aria-labelledby="v-pills-2-tab">
                                <section class="ftco-gallery pb-5 pt-5">
                                    <div class="row no-gutters">
                                        @foreach($medias[1] as $media)
                                            <div class="col-md-3 ftco-animate">
                                                <a href="{{asset($media->name)}}" class="gallery img d-flex align-items-center group"
                                                   style="background-image: url({{asset($media->name)}});">
                                                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                                                        <span class="icon-search"></span>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row mt-5">
                                        <div class="col text-center">
                                            <div id="pagination" class="block-27">
                                                {{ $medias[1]->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="tab-pane fade" id="v-pills-3" role="tabpanel"
                                 aria-labelledby="v-pills-2-tab">
                                <section class="ftco-gallery pb-5 pt-5">
                                    <div class="row no-gutters">
                                        @foreach($medias[2] as $media)
                                            <div class="col-md-3 ftco-animate">
                                                <a href="{{asset($media->name)}}" class="gallery img d-flex align-items-center group"
                                                   style="background-image: url({{asset($media->name)}});">
                                                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                                                        <span class="icon-search"></span>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="row mt-5">
                                        <div class="col text-center">
                                            <div id="pagination" class="block-27">
                                                {{ $medias[2]->links() }}
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- ارتباط با ما --}}
<section class="ftco-appointment">
    <div class="overlay"></div>
    <div class="container-wrap">
        <div class="row no-gutters d-md-flex align-items-center">
            <div class="col-md-6 d-flex align-self-stretch">
                <div style="width: 100%" >
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3235.3525408160276!2d51.42152181526255!3d35.81582518016315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e08a91d9820ef%3A0xd76ef0f9348cba84!2sSaadabad+Historical+Complex!5e0!3m2!1sen!2s!4v1563968377622!5m2!1sen!2s" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6 appointment ftco-animate">
                <h3 class="mb-3 text-right font-force">ارتباط با ما</h3>
                <form action="#" class="appointment-form">
                    <div class="d-md-flex">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="نام ">
                        </div>
                    </div>
                    <div class="d-me-flex">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="نام خانوادگی ">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="" id="" cols="30" rows="3" class="form-control"
                                  placeholder="متن پیام"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="ارسال" class="btn btn-primary py-3 px-4">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>