<html>
<head>
    @include('dashboard.head')
</head>
<body>
@include('dashboard.header')
@include('dashboard.aside')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            گالری
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">گالری</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <div class="col-md-3">
                <a href="{{url("admin/gallery/new/".$cat)}}" class="btn btn-primary btn-block margin-bottom"> اضافه کردن تصویر </a>


                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">دوره ها</h3>

                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="{{$cat=="first" ? 'active' : '' }}"><a href="{{url("admin/gallery/first")}}"><i class="fa fa-inbox"></i> دوره اول</a></li>
                            <li class="{{$cat=="second" ? 'active' : '' }}"><a href="{{url("admin/gallery/second")}}"><i class="fa fa-inbox"></i> دوره دوم</a></li>
                            <li class="{{$cat=="third" ? 'active' : '' }}"><a href="{{url("admin/gallery/third")}}"><i class="fa fa-inbox"></i> دوره سوم</a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">گالری</h3>

                        <div class="box-tools pull-right">
                            <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i
                                        class="fa fa-chevron-right"></i></a>
                            <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i
                                        class="fa fa-chevron-left"></i></a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <!-- /.mailbox-read-info -->
                        <!-- /.mailbox-controls -->
                        <div class="mailbox-read-message">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">تصویر</th>
                                    <th scope="col">حذف</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($medias as $media)
                                <tr>
                                    <th scope="row"><p style="margin-top: 35px;">1</p></th>
                                    <td>
                                        <img style="height: 100px;width:220px;object-fit: cover;" class="img-fluid"
                                             src="{{asset($media->name)}}" alt="">

                                    </td>

                                    <td>
                                        <a href="{{action('MediaController@destroy',['id'=>$media->id,'cat'=>$cat])}}" style="margin-top: 35px;" class="btn btn-danger"><i
                                                    class=" fa fa-minus-circle"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{$medias->links()}}
                        </div>
                        <!-- /.mailbox-read-message -->
                    </div>
                    <!-- /.box-footer -->
                </div>
                <!-- /. box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>

@include('dashboard.footer')
@include('dashboard.script')
</body>


</html>