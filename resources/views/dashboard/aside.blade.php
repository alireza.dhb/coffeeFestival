
  <!-- right side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-right image">
              <img src="{{asset('dashboard/img/avatar.png')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-right info">
              <p>علیرضا حسینی زاده</p>
              <a href="#"><i class="fa fa-circle text-success"></i> آنلاین</a>
            </div>
          </div>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" data-widget="tree">
            <li class="header">منو</li>


            <li class="treeview">
              <a href="#">
                <i class="fa fa-eye"></i> <span>نمایشگاه</span>
                <span class="pull-left-container">
                  <i class="fa fa-angle-right pull-left"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="{{url('admin/events/cafe')}}"><i class="fa fa-circle-o"></i>کافه ها</a></li>
                <li><a href="{{url('admin/events/industry')}}"><i class="fa fa-circle-o"></i>صنایع وابسته</a></li>
                <li><a href="{{url('admin/events/media')}}"><i class="fa fa-circle-o"></i>  رسانه </a></li>
                <li><a href="{{url('admin/events/startup')}}"><i class="fa fa-circle-o"></i>کسب و کار های نوپا</a></li>
                <li><a href="{{url('admin/events/decoration')}}"><i class="fa fa-circle-o"></i>دکوراسیون کافه</a></li>
                <li><a href="{{url('admin/events/tea_coffee_sweets')}}"><i class="fa fa-circle-o"></i>گروه چای، قهوه و شیرینی</a></li>
                <li><a href="{{url('admin/events/pro_courses')}}"><i class="fa fa-circle-o"></i>گروه آموزش دوره های تخصصی</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-eye"></i> <span>رویداد های جانبی</span>
                <span class="pull-left-container">
                  <i class="fa fa-angle-right pull-left"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="{{url('admin/events/concert')}}"><i class="fa fa-circle-o"></i>کنسرت موسیقی</a></li>
                <li><a href="{{url('admin/events/folks_festival')}}"><i class="fa fa-circle-o"></i>جشنواره اقوام</a></li>
                <li><a href="{{url('admin/events/theater')}}"><i class="fa fa-circle-o"></i>  تئاتر کافه </a></li>

              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-calendar"></i> <span>مسابقات</span>
                <span class="pull-left-container">
                  <i class="fa fa-angle-right pull-left"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="{{url('admin/events/good_cafe')}}"><i class="fa fa-circle-o"></i>کافه خوب</a></li>
                <li><a href="{{url('admin/events/cafe_photography')}}"><i class="fa fa-circle-o"></i>عکاسی در کافه</a></li>
                <li><a href="{{url('admin/events/portable')}}"><i class="fa fa-circle-o"></i>کافه سیار</a></li>
                <li><a href="{{url('admin/events/hosting')}}"><i class="fa fa-circle-o"></i>میزبانی در کافه</a></li>
                <li><a href="{{url('admin/events/cooking')}}"><i class="fa fa-circle-o"></i> آشپزی در کافه</a></li>
                <li><a href="{{url('admin/events/cold_bar')}}"><i class="fa fa-circle-o"></i>  بارسرد </a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-trophy"></i> <span>دوره های آموزشی</span>
                <span class="pull-left-container">
                  <i class="fa fa-angle-right pull-left"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="{{url('admin/events/hosting_course')}}"><i class="fa fa-circle-o"></i>میزبانی در کافه</a></li>
                <li><a href="{{url('admin/events/cooking_course')}}"><i class="fa fa-circle-o"></i>آشپزی در کافه</a></li>
                <li><a href="{{url('admin/events/cold_bar_course')}}"><i class="fa fa-circle-o"></i>بار سرد</a></li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-trophy"></i> <span>غیره</span>
                <span class="pull-left-container">
                  <i class="fa fa-angle-right pull-left"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('admin/registration')}}"><i class="fa fa-circle-o"></i>ثبت نام</a></li>
                <li><a href="{{url('admin/events/news')}}"><i class="fa fa-circle-o"></i>اخبار</a></li>
                <li><a href="{{url('admin/gallery/first')}}"><i class="fa fa-circle-o"></i>گالری</a></li>
                <li><a href="{{url('admin/sms')}}"><i class="fa fa-circle-o"></i>sms</a></li>

              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
    