
<!-- jQuery 3 -->
<script src={{URL::asset("dashboard/js/jquery.min.js")}}></script>
<!-- Bootstrap 3.3.7 -->
<script src={{URL::asset("dashboard/js/bootstrap.min.js")}}></script>
<!-- Slimscroll -->
<script src={{URL::asset("dashboard/js/jquery.slimscroll.min.js")}}></script>
<!-- FastClick -->
<script src={{URL::asset("dashboard/js/fastclick.js")}}></script>
<!-- AdminLTE App -->
<script src={{URL::asset("dashboard/js/adminlte.min.js")}}></script>
<!-- AdminLTE for demo purposes -->
<script src={{URL::asset("dashboard/js/demo.js")}}></script>
<!-- iCheck -->
<script src={{URL::asset("dashboard/js/icheck.min.js")}}></script>
<script src={{URL::asset("dashboard/js/tinymce.min.js")}}></script>
<!-- Page Script -->
<script>
    tinymce.init({
        selector: 'textarea#mytextarea',
        plugins : 'advlist autolink link lists preview table code pagebreak',
        menubar: false,
        language: 'fa',
        height: 300,
        relative_urls: false,
        toolbar: 'undo redo | removeformat preview code | fontsizeselect bullist numlist | alignleft aligncenter alignright alignjustify | bold italic | pagebreak table link',
    });


  $(function () {
    //Add text editor
    $("#compose-textarea").wysihtml5();
  });

    if($('#coffeOrSanaye').val()==10){
        $('#dropCoffeOrSanaye').append('<option>گروه کافه، کافی شاپ و کافه قنادی ها و کافه های سیار</option>');
        $('#dropCoffeOrSanaye').append('<option>گروه آموزش دوره های تخصصی و عمومی قهوه و  باریستا</option>');
        $('#dropCoffeOrSanaye').append('<option>گروه محصولات و تجهیزات مرتبط با قهوه</option>');
        $('#dropCoffeOrSanaye').append('<option>گروه دکوراسیون داخلی کافه</option>');
        $('#dropCoffeOrSanaye').append('<option>گروه چای، قهوه، شربت و نوشیدنیها و ... </option>');
        $('#dropCoffeOrSanaye').append('<option>سایر تجهیزات مرتبط</option>');
        $('#dropCoffeOrSanaye').append('<option>کسب و کارهای نوپا مرتبط با کافه </option>');
        $('#dropCoffeOrSanaye').append('<option>محصولات و تجهیزات مرتبط با کافه</option>');
        $('#dropCoffeOrSanaye').append('<option>سایر</option>');
    }else if ($('#coffeOrSanaye').val()==20) {
        $('#dropCoffeOrSanaye').append('<option>کافه</option>');
        $('#dropCoffeOrSanaye').append('<option>کافه زنجیره ای</option>');
        $('#dropCoffeOrSanaye').append('<option>کافه سیار </option>');
    }else if ($('#coffeOrSanaye').val()==30){
        $('#dropCoffeOrSanaye').append('<option>عکاسی در کافه</option>');
        $('#dropCoffeOrSanaye').append('<option>آشپزی در کافه</option>');
        $('#dropCoffeOrSanaye').append('<option>میزبانی در کافه</option>');
    }
    $('#coffeOrSanaye').on('change', function(){
        $('#dropCoffeOrSanaye').html('');
        if($('#coffeOrSanaye').val()==10){
            $('#dropCoffeOrSanaye').append('<option>گروه کافه، کافی شاپ و کافه قنادی ها و کافه های سیار</option>');
            $('#dropCoffeOrSanaye').append('<option>گروه آموزش دوره های تخصصی و عمومی قهوه و  باریستا</option>');
            $('#dropCoffeOrSanaye').append('<option>گروه محصولات و تجهیزات مرتبط با قهوه</option>');
            $('#dropCoffeOrSanaye').append('<option>گروه دکوراسیون داخلی کافه</option>');
            $('#dropCoffeOrSanaye').append('<option>گروه چای، قهوه، شربت و نوشیدنیها و ... </option>');
            $('#dropCoffeOrSanaye').append('<option>سایر تجهیزات مرتبط</option>');
            $('#dropCoffeOrSanaye').append('<option>کسب و کارهای نوپا مرتبط با کافه </option>');
            $('#dropCoffeOrSanaye').append('<option>محصولات و تجهیزات مرتبط با کافه</option>');
            $('#dropCoffeOrSanaye').append('<option>سایر</option>');
        }else if ($('#coffeOrSanaye').val()==20) {
            $('#dropCoffeOrSanaye').append('<option>کافه</option>');
            $('#dropCoffeOrSanaye').append('<option>کافه زنجیره ای</option>');
            $('#dropCoffeOrSanaye').append('<option>کافه سیار </option>');
        }else if ($('#coffeOrSanaye').val()==30){
            $('#dropCoffeOrSanaye').append('<option>عکاسی در کافه</option>');
            $('#dropCoffeOrSanaye').append('<option>آشپزی در کافه</option>');
            $('#dropCoffeOrSanaye').append('<option>میزبانی در کافه</option>');
        }
    });

    // modal
    $('#editModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);// Button that triggered the modal
        let modal = $(this);
        modal.find('.modal-body #name').val(button.data('name'));
        modal.find('.modal-body #agent_name').val(button.data('manager'));
        modal.find('.modal-body #phone').val(button.data('phone'));
        modal.find('.modal-body #booth').val(button.data('booth'));

        modal.find('.modal-body #booth').val(button.data('booth'));
        modal.find('.modal-body #booth').val(button.data('booth'));

        let save = modal.find('.modal-body #id');
        save.val(button.data('id'))
    })
</script>