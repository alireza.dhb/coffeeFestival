<html>
    <head>
        @include('dashboard.head')
    </head>
    <body>
        @include('dashboard.header')
        @include('dashboard.aside')
            
        @include('dashboard.new')

        @include('dashboard.footer')
        @include('dashboard.script')
    </body>


</html>