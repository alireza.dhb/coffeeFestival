<html>
<head>
    @include('dashboard.head')
</head>
<body>
@include('dashboard.header')
@include('dashboard.aside')
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">درخواست غرفه</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table text-center">
                        <thead>
                        <tr>
                            <th scope="col">نام شرکت</th>
                            <th scope="col">نام مسئول</th>
                            <th scope="col">شماره تلفن</th>
                            <th scope="col">غرفه انتخاب شده</th>
                            <th scope="col">تایید پرداخت</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($enrolls as $enroll)
                            <tr>
                                <td>{{$enroll->name}}</td>
                                <td>{{$enroll->manager_name}}</td>
                                <td>{{$enroll->phone}}</td>
                                <td>{{$enroll->booth}}</td>
                                @if($enroll->payment_id==null)
                                    <td>
                                        <div style="width:100%" class="">
                                            <a href="/admin/confirmEnroll?id={{$enroll->id}}" style="width:100%"
                                               class="btn btn-success"> تایید</a>
                                        </div>
                                    </td>
                                @else
                                    <td>
                                        <div style="width:100%" class="">
                                            تایید شده
                                        </div>
                                    </td>
                                @endif
                                <td>
                                    <div style="width:100%" class="">
                                        <div type="button" style="width:100%" class="btn btn-primary"
                                             data-toggle="modal" data-target="#editModal" data-name="{{$enroll->name}}"
                                             data-manager="{{$enroll->manager_name}}"
                                             data-phone="{{$enroll->phone}}" data-booth="{{$enroll->booth}}"
                                             data-id="{{$enroll->id}}">ویرایش
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div style="width:100%" class="">
                                        <a href="/admin/deleteEnroll?id={{$enroll->id}}" style="width:100%"
                                           class="btn btn-danger">حذف</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('admin/updateEnroll')}}" method="post">
                                    @csrf
                                <div class="modal-body">
                                        <input style="display: none;" name="id" id="id">
                                        <div class="form-group">
                                            <label for="name">نام شرکت</label>
                                            <input type="text" class="form-control" id="name" name="name"
                                                   aria-describedby="name" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="agent_name">نام مسئول</label>
                                            <input type="text" class="form-control" id="agent_name" name="manager_name"
                                                   aria-describedby="email" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">شماره تلفن</label>
                                            <input type="tel" class="form-control" id="phone" name="phone"
                                                   aria-describedby="email" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label for="booth">غرفه انتخاب شده</label>
                                            <input type="text" class="form-control" id="booth" name="booth"
                                                   aria-describedby="email" placeholder="">
                                        </div>
{{--
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group text-right">
                                                    <select name="type" class="form-control" id="coffeOrSanaye">
                                                        <option value="20">کافه</option>
                                                        <option value="10">صنایع وابسته</option>
                                                        <option value="30">مسابقات</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group text-right">
                                                    --}}
{{-- در صورتی که صنایع وابسته انتخاب شد در دراپ قبلی--}}{{--

                                                    <select name="sub_type" class="form-control" id="dropCoffeOrSanaye">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
--}}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">بستن</button>
                                    <button  type="submit" class="btn btn-success">ذخیره سازی</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col text-center">
                            <div id="pagination" class="block-27">
                                {{ $enrolls->links() }}
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
@include('dashboard.footer')
@include('dashboard.script')
</body>


</html>