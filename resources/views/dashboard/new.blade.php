<html>
<head>
    @include('dashboard.head')
</head>
<body>
@include('dashboard.header')
@include('dashboard.aside')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            پست ها
            <small>۱۳ پست جدید</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
            <li class="active">پست ها</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3">
                <a href="{{url('admin/events/'.$cat)}}" class="btn btn-primary btn-block margin-bottom">بازگشت</a>

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">پوشه ها</h3>

                        <div class="box-tools">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                        class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <ul class="nav nav-pills nav-stacked">
                            <li class="active"><a href="#"><i class="fa fa-inbox"></i> پست ها</a></li>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /. box -->
                <div style="visibility: hidden" class="box box-solid">
                    <div style="visibility: hidden ; " class="box-header with-border">
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <form method="post" action="{{url('/admin/new/'.$cat)}}" enctype="multipart/form-data">
                @csrf
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">ارسال پست جدید</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <div class="form-group">
                                <input name="title" class="form-control" placeholder="عنوان">
                            </div>
                            <div class="form-group">
                                <input name="description" class="form-control" placeholder="توضیحات">
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <i class="fa fa-paperclip"></i> تصویر اصلی
                                    <input type="file" name="image">
                                </div>
                            </div>
                            <div class="form-group">
                        <textarea name="content" id="mytextarea" class="form-control" style="height: 300px">

                        </textarea>
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                    <i class="fa fa-paperclip"></i> رسانه ها
                                    <input type="file" name="media">
                                </div>
                                <p class="help-block">حداکثر سایز 32MB</p>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="pull-right">
                                {{--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> پیش نویس</button>--}}
                                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> ارسال
                                </button>
                            </div>
                            <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> انصراف</button>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /. box -->
                </div>
            </form>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('dashboard.footer')
@include('dashboard.script')
</body>
</html>