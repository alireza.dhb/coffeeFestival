
  <header class="main-header">
        <!-- Logo -->
        <a href="{{url('admin/registration')}}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">پنل</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>کنترل پنل مدیریت</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src={{asset("dashboard/img/avatar.png")}} class="user-image" alt="User Image">
                  <span class="hidden-xs">علیرضا حسینی زاده</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src={{asset("dashboard/img/avatar.png")}} class="img-circle" alt="User Image">
    
                    <p>
                      علیرضا حسینی زاده
                      <small>مدیریت کل سایت</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body">
                    <div class="row">
                      <div class="col-xs-12 text-center">
                        <a href="#">صفحه من</a>
                      </div>
                    </div>
                    <!-- /.row -->
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat">پروفایل</a>
                    </div>
                    <div class="pull-left">
                      <a href="{{url('admin/logout')}}" class="btn btn-default btn-flat">خروج</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>