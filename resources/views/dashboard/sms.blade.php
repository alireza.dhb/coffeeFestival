<html>
<head>
    @include('dashboard.head')
</head>
<body>
@include('dashboard.header')
@include('dashboard.aside')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            پیام کوتاه
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> خانه</a></li>
        </ol>

        {{--@if($status==200)--}}
            {{--<div class="alert alert-success" style="margin-top: 15px">--}}
                {{--<strong>موفقیت!</strong> پیام های کوتاه ارسال شدند--}}
            {{--</div>--}}
        {{--@endif--}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">

            <!-- /.col -->
            <form method="post" action="{{url('/admin/sms/send')}}" enctype="multipart/form-data">
                @csrf
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">ارسال پیام کوتاه</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">


                            <div class="form-group">
                        <textarea name="content"  class="form-control" style="height: 150px" placeholder="متن پیامک"></textarea>
                            </div>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <div class="pull-right">
                                {{--<button type="button" class="btn btn-default"><i class="fa fa-pencil"></i> پیش نویس</button>--}}
                                <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> ارسال
                                </button>
                            </div>
                            <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> انصراف</button>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                    <!-- /. box -->
                </div>
            </form>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@include('dashboard.footer')
@include('dashboard.script')
</body>
</html>