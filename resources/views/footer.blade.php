@php $footer_events = \App\Http\Controllers\EventController::latest(2) @endphp
<footer style="padding-bottom: 50px;" class="ftco-footer ftco-section img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row mb-5 text-right">
            <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2 font-force">درباره ی ما</h2>
                    <p> به مناسبت یکصد و سیزدهمین سال افتتاح اولین کافه ایران ( کافه لقانطه) سومین جشنواره و نمایشگاه ملی کافه، مقصد گردشگری شهری با حمایت سازمان میراث فرهنگی، صنایع دستی و گردشگری و ستاد گردشگری شهرداری تهران در تهران  ( مجموعه فرهنگی کاخ سعد آباد) برگزار می گردد</p>
                    <ul class="ftco-footer-social list-unstyled float-md-right mt-5 pr-0">
                        <li class="ftco-animate"><a href="{{url('https://www.instagram.com/p/B0Lnnj6pC7K/?igshid=z1l5n112hs9q')}}"><span class="icon-instagram"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2  font-force">جدیدترین های رویداد</h2>
                    @foreach($footer_events as $footer_event)
                        <div class="block-21 mb-4 d-flex">
                            <a class="blog-img mr-4" style="background-image: url(images/image_1.jpg);"></a>
                            <div class="text pr-4 pt-3">
                                <h3 class="heading"><a class=" font-force"
                                                       href={{url($footer_event->id)}}>{{$footer_event->title}}</a></h3>
                                <div class="meta">
                                    <div><a href="#"> 1396 دی 22</a></div>
                                    <div><a href="#">{{$footer_event->created_by}}</a></div>
                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    {{--<div class="block-21 mb-4 d-flex">--}}
                    {{--<a class="blog-img mr-4" style="background-image: url(images/image_1.jpg);"></a>--}}
                    {{--<div class="text pr-4 pt-3">--}}
                    {{--<h3 class="heading">--}}
                    {{--<a class="font-force" href="#">دنیای موجود طراحی اساسا مورد</a>--}}
                    {{--</h3>--}}
                    {{--<div class="meta">--}}
                    {{--<div><a href="#"> 1396 دی 22</a></div>--}}
                    {{--<div><a href="#"> رضا صادقی</a></div>--}}
                    {{--<div><a href="#"><span class="icon-chat"></span> 19</a></div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2 font-force">اطلاعات ما</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text"> کاخ سعد آباد
                                    </span>
                            </li>
                            <li><span class="icon icon-phone"></span><span class="text">021-88442299</span>
                            </li>
                            <li><a href="#"><span  class="icon icon-envelope"></span><span
                                            style="    direction: ltr;"
                                            class="text">@cafefestival.ir</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <p>
                    Powered by CDH and <a href="http://www.diacostudios.com">Diaco</a></p>
            </div>
        </div>
    </div>
</footer>


<!-- loader -->
<div id="ftco-loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/>
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00"/>
    </svg>
</div>
