<html>
<head>
    @include('head')
</head>
<body>
@include('header')
<section class="ftco-intro mt-5">
    <div style="
    border: 1px solid #444444;
    border-radius: 20px;" class="container mb-5">
    <div class="row text-center">
        <div class="col-md-12">
            <h2 class="font-force-title mt-5 mb-4 text-center"> تایید اطلاعات </h2>
        </div>
        <div class="col-md-6"><h5 class="font-force">نام غرفه : {{$enroll->name}}</h5></div>
        <div class="col-md-6"><h5 class="font-force">نام مسئول : {{$enroll->name}}</h5></div>
        <div class="col-md-6"><h5 class="font-force"> تلفن همراه : {{$enroll->phone}}</h5></div>
        <div class="col-md-6"><h5 class="font-force"> غرفه انتخاب شده : {{$enroll->booth}}</h5></div>

                    <div id="mapSelected" class="text-center col-md-12  mt-5 mb-5">
                        <a href="{{url('confirm?id='.$enroll->id)}}" style="background: #2dc14d;border: 1px solid #43b31f;color: #fff;" type="submit" class="btn btn-primary text-right">ثبت نهایی </a>
                    </div>
    </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>