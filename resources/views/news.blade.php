<html>
<head>
    @include('head')
</head>
<body>
@include('header')
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <h1 class="mb-4 font-force">اخبار</h1>
                <p>اخبار مربوط به جشنواره </p>
            </div>
        </div>
        <div class="row d-flex">
            @foreach ($events as $event)
                <div class="col-md-4 d-flex ftco-animate">
                    <div class="blog-entry align-self-stretch">
                        <a href="{{url("event/".$event->id)}}" class="block-20"
                           style="background-image: url({{asset($event->image)}});">
                        </a>
                        <div class="text py-4 d-block  text-right">
                            <div class="meta">
                                <div><a href="">{{$event->created_at}}</a></div>
                            </div>
                            <h3 class="heading mt-2"><a class="font-force" href="{{url("event/".$event->id)}}">{{$event->title}}</a>
                            </h3>
                            <p>{{$event->description}}</p>
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="row mt-5">
                <div class="col text-center">
                    <div id="pagination" class="block-27">
                        {{ $events->links() }}
                    </div>
                </div>
            </div>
            {{--<div class="row mt-5">--}}
                {{--<div class="col text-center">--}}
                    {{--<div class="block-27">--}}
                        {{--<ul>--}}
                            {{--<li><a href="#">&lt;</a></li>--}}
                            {{--<li class="active"><span>1</span></li>--}}
                            {{--<li><a href="#">2</a></li>--}}
                            {{--<li><a href="#">3</a></li>--}}
                            {{--<li><a href="#">4</a></li>--}}
                            {{--<li><a href="#">5</a></li>--}}
                            {{--<li><a href="#">&gt;</a></li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>