<html>
<head>
    @include('head')
</head>
<body>
@include('header')
{{-- اطاعات --}}
<section class="ftco-intro">
    <div class="container-wrap">
        <div class="wrap d-md-flex">
            <div class="info">
                <div class="row no-gutters">
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-phone"></span></div>
                        <div class="text text-right pr-3">
                            <h3 class="font-force">021-88442299</h3>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-my_location"></span></div>
                        <div class="text text-right pr-3">
                            <h3 class="font-force">تهران </h3>
                            <p style="font-size: 12px;">خیابان ولیعصر، خیابان شهید فلاحی ( زعفرانیه ) انتهای خیابان شهید کمال طاهری
                                مجموعه فرهنگی تاریخی سعدآباد</p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-clock-o"></span></div>
                        <div class="text text-right pr-3">
                            <h3 class="font-force">3 تا 8 شهریور</h3>
                            <p>ساعت 16 الی 22</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="social d-md-flex pl-md-5 p-4 align-items-center">
                <ul class="social-icon w-100">
                    <li class="ftco-animate">
                        <a href="http://www.instagram.com/cafefestival">
                            <p style="color: white;display: inline;margin-left: 60px;font-size: 17px;padding-bottom: 12px;">
                                <span style="font-size: 25px;" class="icon-instagram pl-5"></span>به اینستاگرام ما سر
                                بزنید</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
{{--  درباره ی ما  --}}
<section class="ftco-about d-md-flex">
    <div class="one-half img" style="background-image: url({{url("images/gallery/gallery9.jpg")}});"></div>
    <div class="one-half ftco-animate text-right">
        <div class="heading-section ftco-animate">
            <h5 class="mb-2 font-force"> فراخوان سومین جشنواره و نمایشگاه کافه مقصد گردشگری شهری</h5>
        </div>
        <div>
            <p style="
    font-size: 13px;color:#9a9a9a;">میراث هر سرزمینی تنها شامل ابنیه و آثار کهن آن نمی شود بلکه هر اثری که به خاطره تبدیل شود بخشی از میراث انسانی را تشکیل می دهد و بر همین قیاس می توان نخستین کافه ها را به عنوان نهادی مردمی و پدیده ای شهری، که  در خلق خاطرات مشترک شهروندان و نسل ها نقش تاثیرگذاری داشته اند در شمار این میراث قلمداد کرد.
                جای بسی خوشحالی و خوشوقتی است که نخستین کافه ایرانی (لقانطه) پس از یکصد و ده سال باز زنده سازی شده و موجب و موجد جریانی فرهنگی، هنری در قالب جشنواره ای تحت عنوان کافه مقصد گردشگری شهری گشته است.
                بی شک نقش کافه ها در شکل گیری جنبش های ادبی، فرهنگی و هنری چه در جهان و چه در ایران انکار ناپذیر است و این امر نه تنها در تهران بلکه در برخی از کلان شهرها و شهرستان های دیگر نیز زمینه های خاطره انگیزی را بر همان رویه ایجاد کرده است.
                شعر، موسیقی، سینما، تئاتر و نمایش به عنوان میراث ناملموس، پایگاه و جایگاهی در کافه ها داشته که هر کدام در نوع خود منظری از گردشگری شهری را در اشکال نوین می تواند تبیین کند.
                امیدوارم این حرکت فرهنگی هنری که پیشینه ی تاریخی قابل تعمقی را نیز در سابقه ی خود دارد بتواند زمینه حفظ، صیانت و احیا خاطرات شهری و هویت فرهنگی را فراهم آورد.
                حال پس از دو دوره برگزاری جشنواره و نمایشگاه ملی کافه مقصد گردشگری شهری، می توان به نقش بخش خصوصی در احیا و حفظ میراث فرهنگی ملموس و غیر ملموس امیدوار بود.
                بخشی از دستاوردهای این حرکت فرهنگی، توجه کارآفرینان به ساختمانهای قدیمی و تاریخی و احیا بیش از 120 ساختمان در معرض تخریب و تبدیل آن به کافه ها، رستورانها و به نوعی پاتوق های فرهنگی، هنری، اجتماعی و استفاده از آنها به عنوان مقاصد گردشگری شهری است.
                امروز می توان با افتخار بیان نمود که نقش بخش خصوصی در این زمینه جدی تر از فعالیتهای دولتی و حاکمیتی بروز و ظهور پیدا می کند.
                در راستای فعالیت های تخصصی صنعت کافه و کافه داری، در ادوار گذشته با محوریت و رویکرد بهبود کیفیت خدمات در محصولات ارائه شده مسابقات ملی باریستا و قهوه نگاری(لته آرت) و ... صورت پذیرفت که موفقیتهای قابل توجهی را بدست آورد.
                اما نکته قابل تامل و تعمق اینکه در صنعت گردشگری و خدمات پذیرایی، کم توجهی به امر میزبانی و اهمیت آن در مجموعه های یاد شده مشهود است.
                امید است در سومین جشنواره و نمایشگاه بتوانیم توجه کارآفرینان، صاحبان کافه ها و کارکنان را به بهبود خدمات میزبانی و جایگاه آن در میزان رضایتمندی میهمانان و گردشگران جلب نماییم و این نهضت شروعی باشد برای تبدیل شدن ایران عزیز به قطب گردشگری کشورهای اسلامی.
                با همان شعار همیشگی " میهمان نوازی ایرانی" .
                پاینده باد ایران
            </p>
        </div>
    </div>
</section>
{{-- اساتید --}}
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-4 font-force">اساتید ما</h2>
                <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
                <p class="mt-5">در این بخش به معرفی کوتاهی از اساتید میپردازیم</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 d-flex mb-sm-6 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url({{asset("images/staff/pishro.png")}});"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="{{url('pishro')}}">(علا ) نصرالله پیشرو</a></h3>
                        <span class="position">استاد آشپزی در کافه</span>
                        <div class="text">
                            <p>همکاری در راه اندازی رستورانها وکیترینگهای مختلف(فود لند کیش- مجتمع بین راهی راغب گرمسار،  شبهای تهران- پیتزا هفت شب- فود کورت بل مارکت اصفهان- کیترینگ هانی- سامانه همایش علا-ویکولو- مجتمع مهتاب و مهروماه قم،بیاوبرو پیتزا)
                                - مدیریت و کنترل  مراسم پذیرائی از سفرای کشورهای خارجی در تالار آبگینه وزارت امور خارجه
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex mb-sm-6 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url({{asset("images/staff/bidarmaghz.png")}});"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="{{url('bidarmaghz')}}">استاد علی محمد بیدارمغز</a></h3>
                        <span class="position">سخنران و مدرس تشریفات کاربردی بین المللی</span>
                        <div class="text">
                            <p>- رایزن اول کادر سیاسی(بازنشسته) – وزارت امور خارجه جمهوری اسلامی ایران
                                - دبیرسوم، سفارت جمهوری اسلامی ایران
                                - کاردار اسبق و نفر دوم سرپرست امور دانشجویی و رییس روابط عمومی سفارت ایران
                                - دبیر اول و رییس بخش اقتصادی سفارت جمهوری اسلامی ایران
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 d-flex mb-sm-6 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url({{asset("images/staff/safaee.png")}});"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="{{url('safaee')}}">حافظ صفایی</a></h3>
                        <span class="position">داور مسابقات</span>
                        <div class="text">
                            <p>موسس و مديريت برند زنجیره ای کرپ کافه - در حال حاضر ۸ شعبه ی‌فعال در تهران - اهواز ، بندر عباس و کشور عمان
                                مدرس و موسس‌برند مولکولار میکسولوژی‌ و آموزش‌تخصصی نوشیدنی های سرد
                                دبیر برگزاری و سرداور مسابقات  میکسولوژی مولکولی در باغ موزه قصر - اردیبهشت ۹۷
                                مدرس دوره های تخصصی قهوه از مزرعه تا فنجان
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- مسئولین اجرایی --}}
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-4 font-force">مسئولین اجرایی </h2>
                <p class="flip"><span class="deg1"></span><span class="deg2"></span><span class="deg3"></span></p>
                <p class="mt-5">در این بخش به معرفی کوتاهی از مسئولین میپردازیم</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">حمید شاهین پور</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول برگزاری</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">محمد پیروزرام</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مدیر ارتباطات و رسانه
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">سهیل توتونچی</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول بازرگانی</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">

                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">خانم هنرمند</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول بخش نمایشگاهی</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">

                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">بهروز خواجه نوری</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول فناوری اطلاعات</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">اکبر بیاتی</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول ستاد خبری</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">خانم شاهده یوسفی</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>عضو ستاد خبری</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">

                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">رضا اردانی</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول هماهنگی</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">

                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">مصطفی بی غم</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول پشتیبانی</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">

                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">میلاد پیروزرام</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول تولید محتوا</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 d-flex mb-sm-4 ftco-animate">
                <div class="staff">
                    <div class="img mb-4" style="background-image: url(images/emptyImage.jpg);"></div>
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">خانم حسن بیگی</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>مسئول تشریفات</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 d-flex mb-sm-4 ftco-animate">
                <div style="
    padding-top: 10%;" class="staff w-100">
                    <div class="info text-center">
                        <h3><a class="font-force" href="#">شورای سیاست گذاری از تاسیس تا کنون</a></h3>
                        <span class="position"></span>
                        <div class="text">
                            <p>سید محمد بهشتی , سید احمد محیط طباطبایی , علیرضا قلی نژاد پیربازاری , ادشیر صالح پور , مجید میرفخرایی , احمد مسجد جامعی , رجبعلی خسرو آبادی , دلاور بزرگ نیا , کیا پارسا , مهدی سیف , حجت نظری و زنده یاد استاد جمشید مشایخی</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- همراهان ما --}}
<section class="ftco-section ftco-services">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <h2 class="mb-4 font-force">همراهان ما</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2 ftco-animate">
                <div class="media d-block text-center block-6 services">
                    <div class="icon d-flex justify-content-center align-items-center mb-5">
                        <img class="img-fluid" src={{url("images/sponser/logo1.png")}} alt="">
                    </div>
                    <div class="media-body">
                        <h3 class="heading font-force"> سازمان فرهنگی هنری شهرداری تهران</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2 ftco-animate">
                <div class="media d-block text-center block-6 services">
                    <div class="icon d-flex justify-content-center align-items-center mb-5">
                        <img class="img-fluid" src="{{url("images/sponser/logo2.jpg")}}" alt="">
                    </div>
                    <div class="media-body">
                        <h3 class="heading font-force">سازمان صنایع دستی و میراث فرهنگی </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2 ftco-animate">
                <div class= "media d-block text-center block-6 services">
                    <div class="icon d-flex justify-content-center align-items-center mb-5">
                        <img class="img-fluid" src="{{url("images/sponser/logo3.png")}}" alt="">
                    </div>
                    <div class="media-body">
                        <h3 class="heading font-force">باغ موزه قصر </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2 ftco-animate">
                <div class="media d-block text-center block-6 services">
                    <div class="icon d-flex justify-content-center align-items-center mb-5">
                        <img class="img-fluid" src="{{url("images/sponser/logo4.png")}}" alt="">
                    </div>
                    <div class="media-body">
                        <h3 class="heading font-force">ستاد گردشگری شهرداری تهران </h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2 ftco-animate">
                <div class="media d-block text-center block-6 services">
                    <div class="icon d-flex justify-content-center align-items-center mb-5">
                        <img class="img-fluid" src="{{url("images/sponser/logo5.png")}}" alt="">
                    </div>
                    <div class="media-body">
                        <h3 class="heading font-force">ایده پردازان آماج</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-2 ftco-animate">
                <div class="media d-block text-center block-6 services">
                    <div class="icon d-flex justify-content-center align-items-center mb-5">
                        <img class="img-fluid" src="{{url("images/sponser/logo6.png")}}" alt="">
                    </div>
                    <div class="media-body">
                        <h3 class="heading font-force">کافه لقانطه</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- تماس با ما --}}
<section class="ftco-appointment">
    <div class="overlay"></div>
    <div class="container-wrap">
        <div class="row no-gutters d-md-flex align-items-center">
            <div class="col-md-6 d-flex align-self-stretch">
                <div style="width: 100%" >
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3235.3525408160276!2d51.42152181526255!3d35.81582518016315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e08a91d9820ef%3A0xd76ef0f9348cba84!2sSaadabad+Historical+Complex!5e0!3m2!1sen!2s!4v1563968377622!5m2!1sen!2s" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6 appointment ftco-animate">
                <h3 class="mb-3 text-right font-force">ارتباط با ما</h3>
                <form action="#" class="appointment-form">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="نام شما ">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="ایمیل">
                            </div>
                        </div>
                    </div>
                    <div class="d-me-flex">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="نام خانوادگی ">
                        </div>
                    </div>
                    <div class="form-group">
                                <textarea name="" id="" cols="30" rows="3" class="form-control"
                                          placeholder="متن پیام"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="ارسال" class="btn btn-primary py-3 px-4">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>