<html>
<head>
    @include('head')
</head>
<body>
@include('header')
<div class="alert alert-warning alert-dismissible mx-2" style="direction: rtl;text-align: right;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>همراهان و مخاطبین محترم.</strong>
    <br/>
    سومین جشنواره و نمایشگاه ملی کافه مقصد گردشگری شهری بدلیل تقارن با هفته دولت و برگزاری این مراسم در مجموعه تاریخی فرهنگی سعد آباد به وقت دیگری موکول میگردد.
    در اسرع وقت تاریخ برگزاری اطلاع رسانی خواهد شد.
    <br/>
    حمید شاهین پور مدیر جشنواره و نمایشگاه ملی کافه
</div>
<section class="home-slider owl-carousel img" style="background-image: url(/images/bg11.jpg);">
    <!--      <div class="slider-item">-->
    <!--      	<div class="overlay"></div>-->
    <!--        <div class="container">-->
    <!--          <div class="row slider-text align-items-center" data-scrollax-parent="true">-->
    <!--            <div class="col-md-6 col-sm-12 ftco-animate">-->
    <!--            	<span class="subheading font-force">خوش آمدید</span>-->
    <!--              <h1 class="mb-4 font-force">فستیوال کافه</h1>-->
    <!--              <p style="line-height: 31px;" class="mb-4 mb-md-5">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است</p>-->
    <!--              <p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">ثبت نام</a> <a href="#" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">اطلاعات بیشتر</a></p>-->
    <!--            </div>-->
    <!--            <div class="col-md-6 ftco-animate">-->
    <!--            	<img src="images/coffecup.png" class="img-fluid" alt="">-->
    <!--            </div>-->

    <!--          </div>-->
    <!--        </div>-->
    <!--      </div>-->

    <div class="slider-item" style="background-image: url(/images/bg11.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
                <div class="col-md-7 col-sm-12 text-center ftco-animate">
                    <span class="subheading font-force-title">خوش آمدید</span>
                    <h1 class="mb-4 font-force-title"> فراخوان سومین جشنواره و نمایشگاه کافه مقصد گردشگری شهری</h1>
                    {{--                    <p style="direction:rtl" class="mb-4 mb-md-5 ">در اقتصاد نوین جهانی، گردشگری شهری نقش بسزایی در رشد اقتصادی و فرهنگی جوامع ایفا  می کند و لازمه ارتقاء سطح گردشگری در جوامع علاوه بر شناسایی توانمندی و جاذبه های فرهنگی، افزایش امکانات و زیرساخت های اساسی این صنعت است. . .</p>--}}
                    <p><a href="{{url('booth')}}" class="btn btn-primary p-3 px-xl-4 py-xl-3">ثبت نام</a> <a href="{{url('invitation')}}"
                                                                                              class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">اطلاعات
                            بیشتر </a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-intro">
    <div class="container-wrap">
        <div class="wrap d-md-flex">
            <div class="info">
                <div class="row no-gutters">
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-phone"></span></div>
                        <div class="text text-right pr-3">
                            <h3 class="font-force">021-88442299</h3>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-my_location"></span></div>
                        <div class="text text-right pr-3">
                            <h3 class="font-force">تهران </h3>
                            <p style="font-size: 12px;"> کاخ سعد آباد</p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex ftco-animate">
                        <div class="icon"><span class="icon-clock-o"></span></div>
                        <div class="text text-right pr-3">
                            <h3 class="font-force">3 تا 8 شهریور</h3>
                            <p>ساعت 16 الی 22</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="social d-md-flex pl-md-5 p-4 align-items-center">
                <ul class="social-icon w-100">
                    <li class="ftco-animate">
                        <a href="https://www.instagram.com/p/B0Lnnj6pC7K/?igshid=z1l5n112hs9q">
                            <p style="color: white;display: inline;font-size: 17px;padding-bottom: 12px;">
                                <span style="font-size: 25px;" class="icon-instagram pl-5"></span>به اینستاگرام ما سر
                                بزنید</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="ftco-about d-md-flex">
    <div class="one-half img" style="background-image: url(/images/gallery/gallery9.jpg);"></div>
    <div class="one-half ftco-animate text-right">
        <div class="heading-section ftco-animate">
            <h5 class="mb-2 font-force-title"> فراخوان سومین جشنواره و نمایشگاه کافه مقصد گردشگری شهری</h5>
        </div>
        <div>

            <p style="
    font-size: 13px;color:#9a9a9a;">میراث هر سرزمینی تنها شامل ابنیه و آثار کهن آن نمی شود بلکه هر اثری که به خاطره تبدیل شود بخشی از میراث انسانی را تشکیل می دهد و بر همین قیاس می توان نخستین کافه ها را به عنوان نهادی مردمی و پدیده ای شهری، که  در خلق خاطرات مشترک شهروندان و نسل ها نقش تاثیرگذاری داشته اند در شمار این میراث قلمداد کرد.
                جای بسی خوشحالی و خوشوقتی است که نخستین کافه ایرانی (لقانطه) پس از یکصد و ده سال باز زنده سازی شده و موجب و موجد جریانی فرهنگی، هنری در قالب جشنواره ای تحت عنوان کافه مقصد گردشگری شهری گشته است.
                بی شک نقش کافه ها در شکل گیری جنبش های ادبی، فرهنگی و هنری چه در جهان و چه در ایران انکار ناپذیر است و این امر نه تنها در تهران بلکه در برخی از کلان شهرها و شهرستان های دیگر نیز زمینه های خاطره انگیزی را بر همان رویه ایجاد کرده است.
                شعر، موسیقی، سینما، تئاتر و نمایش به عنوان میراث ناملموس، پایگاه و جایگاهی در کافه ها داشته که هر کدام در نوع خود منظری از گردشگری شهری را در اشکال نوین می تواند تبیین کند.
                امیدوارم این حرکت فرهنگی هنری که پیشینه ی تاریخی قابل تعمقی را نیز در سابقه ی خود دارد بتواند زمینه حفظ، صیانت و احیا خاطرات شهری و هویت فرهنگی را فراهم آورد.
                حال پس از دو دوره برگزاری جشنواره و نمایشگاه ملی کافه مقصد گردشگری شهری، می توان به نقش بخش خصوصی در احیا و حفظ میراث فرهنگی ملموس و غیر ملموس امیدوار بود.
                بخشی از دستاوردهای این حرکت فرهنگی، توجه کارآفرینان به ساختمانهای قدیمی و تاریخی و احیا بیش از 120 ساختمان در معرض تخریب و تبدیل آن به کافه ها، رستورانها و به نوعی پاتوق های فرهنگی، هنری، اجتماعی و استفاده از آنها به عنوان مقاصد گردشگری شهری است.
                امروز می توان با افتخار بیان نمود که نقش بخش خصوصی در این زمینه جدی تر از فعالیتهای دولتی و حاکمیتی بروز و ظهور پیدا می کند.
                در راستای فعالیت های تخصصی صنعت کافه و کافه داری، در ادوار گذشته با محوریت و رویکرد بهبود کیفیت خدمات در محصولات ارائه شده مسابقات ملی باریستا و قهوه نگاری(لته آرت) و ... صورت پذیرفت که موفقیتهای قابل توجهی را بدست آورد.
                اما نکته قابل تامل و تعمق اینکه در صنعت گردشگری و خدمات پذیرایی، کم توجهی به امر میزبانی و اهمیت آن در مجموعه های یاد شده مشهود است.
                امید است در سومین جشنواره و نمایشگاه بتوانیم توجه کارآفرینان، صاحبان کافه ها و کارکنان را به بهبود خدمات میزبانی و جایگاه آن در میزان رضایتمندی میهمانان و گردشگران جلب نماییم و این نهضت شروعی باشد برای تبدیل شدن ایران عزیز به قطب گردشگری کشورهای اسلامی.
                با همان شعار همیشگی " میهمان نوازی ایرانی" .
                پاینده باد ایران
            </p>
        </div>
    </div>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="col-md-12 text-center pb-4">
            <h3 class="text-center font-force-title">سومین دوره</h3>
            <div class="w-100">
                <video class="js-player"  poster="/path/to/poster.jpg" id="player" playsinline >
                    <source src="videos/sevominDooreh.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
    </div>
{{--    <div class="container-wrap">--}}
{{--        <div class="row no-gutters d-flex">--}}
{{--            @foreach($latest_events as $event)--}}
{{--                <div class="col-lg-4 d-flex ftco-animate">--}}
{{--                    <div class="services-wrap d-flex">--}}
{{--                        <a href="{{url("event/".$event->id)}}" class="img"--}}
{{--                           style="background-image: url(/images/makecoffe1.jpg);"></a>--}}
{{--                        <div class="text   text-right p-4">--}}
{{--                            <h5 class="font-force">{{$event->title}}</h5>--}}
{{--                            <p>{{$event->description}}</p>--}}
{{--                            <p class="price"><a href="{{url("event/".$event->id)}}" class="ml-2 btn btn-white btn-outline-white">اطلاعات--}}
{{--                                    بیشتر /--}}
{{--                                    ثبت--}}
{{--                                    نام</a></p>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--        </div>--}}
{{--        <div class="text-center w-100">--}}
{{--            <a href="{{ url('events') }}"--}}
{{--               class="btn btn-white text-center mt-3 btn-outline-white p-3 px-xl-4 py-xl-3">رویداد های--}}
{{--                بیشتر</a>--}}
{{--        </div>--}}
{{--    </div>--}}
</section>

<section class="ftco-gallery">
    <div class="container-wrap">
        <h2 class="mb-4 font-force-title text-center pb-3">گالری</h2>
        <div class="row no-gutters">
            <div class="col-md-3 ftco-animate">
                <a href="{{asset('images/gallery/gallery1.jpg')}}" class="gallery img d-flex align-items-center group"
                   style="background-image: url(images/gallery/gallery1.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ftco-animate">
                <a href="{{asset('images/gallery/gallery2.jpg')}}" class="gallery img d-flex align-items-center group"
                   style="background-image: url(images/gallery/gallery2.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ftco-animate">
                <a href="{{asset('images/gallery/gallery4.jpg')}}" class="gallery img d-flex align-items-center group"
                   style="background-image: url(images/gallery/gallery4.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="col-md-3 ftco-animate">
                <a href="{{asset('images/gallery/gallery5.jpg')}}" class="gallery img d-flex align-items-center group"
                   style="background-image: url(images/gallery/gallery5.jpg);">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-search"></span>
                    </div>
                </a>
            </div>
            <div class="text-center w-100 mb-3">
                <a href="{{url("gallery")}}" class="btn btn-white text-center mt-3 btn-outline-white p-3 px-xl-4 py-xl-3">عکس های
                    بیشتر</a>
            </div>
        </div>
    </div>
</section>
{{--<section class="ftco-section">--}}
    {{--<div class="container">--}}
        {{--<div class="row justify-content-center mb-5 pb-3">--}}
            {{--<div class="col-md-7 heading-section ftco-animate text-center">--}}
                {{--<h2 class="mb-4 font-force-title">پرطرفدار ترین مسابقات</h2>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row d-flex">--}}
            {{--<div class="col-md-4 d-flex ftco-animate">--}}
                {{--<div class="blog-entry align-self-stretch">--}}
                    {{--<a href="single.html" class="block-20" style="background-image: url('images/image_1.jpg');">--}}
                    {{--</a>--}}
                    {{--<div class="text py-4 d-block  text-right">--}}
                        {{--<div class="meta">--}}
                            {{--<div><a href="#"> 1396 دی 22 /</a></div>--}}
                            {{--<div><a href="#">رضا صادقی</a></div>--}}
                            {{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
                        {{--</div>--}}
                        {{--<h3 class="heading mt-2"><a class="font-force" href="#">دنیای موجود طراحی اساسا مورد</a></h3>--}}
                        {{--<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک--}}
                            {{--است</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4 d-flex ftco-animate">--}}
                {{--<div class="blog-entry align-self-stretch">--}}
                    {{--<a href="single.html" class="block-20" style="background-image: {{url('/images/image_2.jpg')}}">--}}
                    {{--</a>--}}
                    {{--<div class="text py-4 d-block text-right">--}}
                        {{--<div class="meta">--}}
                            {{--<div><a href="#"> 1396 دی 22 /</a></div>--}}
                            {{--<div><a href="#">رضا صادقی</a></div>--}}
                            {{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
                        {{--</div>--}}
                        {{--<h3 class="heading mt-2"><a class="font-force" href="#">دنیای موجود طراحی اساسا مورد</a></h3>--}}
                        {{--<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک--}}
                            {{--است</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md-4 d-flex ftco-animate">--}}
                {{--<div class="blog-entry align-self-stretch">--}}
                    {{--<a href="single.html" class="block-20" style="background-image: url('images/image_3.jpg');">--}}
                    {{--</a>--}}
                    {{--<div class="text py-4 d-block  text-right">--}}
                        {{--<div class="meta">--}}
                            {{--<div><a href="#"> 1396 دی 22 /</a></div>--}}
                            {{--<div><a href="#">رضا صادقی</a></div>--}}
                            {{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
                        {{--</div>--}}
                        {{--<h3 class="heading mt-2"><a class="font-force" href="#">دنیای موجود طراحی اساسا مورد</a></h3>--}}
                        {{--<p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک--}}
                            {{--است</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</section>--}}

{{--<section class="ftco-counter ftco-bg-dark img" id="section-counter" style="background-image: url(images/bg_2.jpg);"--}}
{{--         data-stellar-background-ratio="0.5">--}}
{{--    <div class="overlay"></div>--}}
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-10">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <div class="icon"><span class="flaticon-laugh "></span></div>--}}
{{--                                <strong class="number" data-number="1200">0</strong>--}}
{{--                                <span>شرکت کننده </span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <div class="icon"><span class="flaticon-shop"></span></div>--}}
{{--                                <strong class="number" data-number="85">0</strong>--}}
{{--                                <span>کافه های شرکت کرده</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <div class="icon"><span class="flaticon-food-and-restaurant"></span></div>--}}
{{--                                <strong class="number" data-number="100">0</strong>--}}
{{--                                <span>رویداد </span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <div class="icon"><span class="flaticon-chef"></span></div>--}}
{{--                                <strong class="number" data-number="100">0</strong>--}}
{{--                                <span>اساتید</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}

<section class="ftco-appointment">
    <div class="overlay"></div>
    <div class="container-wrap">
        <div class="row no-gutters d-md-flex align-items-center">
            <div class="col-md-6 d-flex align-self-stretch">

                <div style="width: 100%" >
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3235.3525408160276!2d51.42152181526255!3d35.81582518016315!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8e08a91d9820ef%3A0xd76ef0f9348cba84!2sSaadabad+Historical+Complex!5e0!3m2!1sen!2s!4v1563968377622!5m2!1sen!2s" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-6 appointment ftco-animate">
                <h3 class="mb-3 text-right font-force">ارتباط با ما</h3>
                <form action="#" class="appointment-form">
                    <div class="d-md-flex">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="نام ">
                        </div>
                    </div>
                    <div class="d-me-flex">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="نام خانوادگی ">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="" id="" cols="30" rows="3" class="form-control"
                                  placeholder="متن پیام"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="ارسال" class="btn btn-primary py-3 px-4">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>