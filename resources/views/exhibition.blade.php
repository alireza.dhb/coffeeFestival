<html>
<head>
    @include('head')
</head>
<body>
@include('header')

<section class="ftco-menu">
    <div class="container-fluid">
        <div class="row d-md-flex">
            <div class="col-lg-12 ftco-animate p-md-5">
                <div class="row">
                    <div class="col-md-12 nav-link-wrap mb-5">
                        <div class="nav ftco-animate nav-pills" id="v-pills-tab" role="tablist"
                             aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab"
                               aria-controls="v-pills-1" aria-selected="true">کافه ها</a>
                            <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab"
                               aria-controls="v-pills-2" aria-selected="false"> صنایع وابسته</a>
                            <a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab"
                               aria-controls="v-pills-3" aria-selected="false">رسانه ها</a>
                            <a class="nav-link" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab"
                               aria-controls="v-pills-4" aria-selected="false">کسب و کار های نوپا</a>
                            <a class="nav-link" id="v-pills-5-tab" data-toggle="pill" href="#v-pills-5" role="tab"
                               aria-controls="v-pills-5" aria-selected="false"> دکوراسیون کافه</a>
                            <a class="nav-link" id="v-pills-6-tab" data-toggle="pill" href="#v-pills-6" role="tab"
                               aria-controls="v-pills-6" aria-selected="false">گروه چای , قهوه و شیرینی</a>
                            <a class="nav-link" id="v-pills-7-tab" data-toggle="pill" href="#v-pills-7" role="tab"
                               aria-controls="v-pills-7" aria-selected="false">گروه آموزش دوره های تخصصی </a>

                        </div>
                    </div>
                    <div class="col-md-12 d-flex align-items-center">

                        <div class="tab-content ftco-animate" id="v-pills-tabContent">

                            <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel"
                                 aria-labelledby="v-pills-1-tab">
                                <div class="row">
                                    @foreach($events[0] as $event)
                                    <div class="col-md-4 pt-1 text-center">
                                        <div class="menu-wrap">
                                            <a href="{{url('event/'.$event->id)}}" class="menu-img img mb-4"
                                               style="background-image: url({{asset($event->image)}});"></a>
                                            <div class="text">
                                                <h3><a class="font-force" href="{{url('event/'.$event->id)}}">{{$event->title}}</a></h3>
                                                <p>{{$event->description}}</p>
                                                <p><a href="{{url('event/'.$event->id)}}" class="btn btn-white btn-outline-white">اطلاعات بیشتر /
                                                        ثبت نام</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <div id="pagination" class="block-27">
                                            {{ $events[0]->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
                                <div class="row">
                                    @foreach($events[1] as $event)
                                    <div class="col-md-4 text-center">
                                        <div class="menu-wrap">
                                            <a href="{{url('event/'.$event->id)}}" class="menu-img img mb-4"
                                               style="background-image: url({{asset($event->image)}});"></a>
                                            <div class="text">
                                                <h3><a class="font-force" href="{{url('event/'.$event->id)}}">لورم ایپسوم متن </a></h3>
                                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                                    استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در
                                                    ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                                                    کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                                <p><a href="{{url('event/'.$event->id)}}" class="btn btn-white btn-outline-white">اطلاعات بیشتر /
                                                        ثبت نام</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <div id="pagination" class="block-27">
                                            {{ $events[1]->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
                                <div class="row">
                                    @foreach($events[2] as $event)
                                    <div class="col-md-4 text-center">
                                        <div class="menu-wrap">
                                            <a href="{{url('event/'.$event->id)}}" class="menu-img img mb-4"
                                               style="background-image: url({{asset($event->image)}});"></a>
                                            <div class="text">
                                                <h3><a class="font-force" href="{{url('event/'.$event->id)}}"> لورم ایپسوم متن </a></h3>
                                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                                    استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در
                                                    ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                                                    کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                                <p><a href="{{url('event/'.$event->id)}}" class="btn btn-white btn-outline-white">اطلاعات بیشتر /
                                                        ثبت نام</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <div id="pagination" class="block-27">
                                            {{ $events[2]->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
                                <div class="row">
                                    @foreach($events[3] as $event)
                                    <div class="col-md-4 text-center">
                                        <div class="menu-wrap">
                                            <a href="{{url('event/'.$event->id)}}" class="menu-img img mb-4"
                                               style="background-image: url({{asset($event->image)}});"></a>
                                            <div class="text">
                                                <h3><a class="font-force" href="{{url('event/'.$event->id)}}"> لورم ایپسوم متن </a></h3>
                                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                                    استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در
                                                    ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                                                    کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                                <p><a href="{{url('event/'.$event->id)}}" class="btn btn-white btn-outline-white">اطلاعات بیشتر /
                                                        ثبت نام</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <div id="pagination" class="block-27">
                                            {{ $events[3]->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
                                <div class="row">
                                    @foreach($events[4] as $event)
                                    <div class="col-md-4 text-center">
                                        <div class="menu-wrap">
                                            <a href="{{url('event/'.$event->id)}}" class="menu-img img mb-4"
                                               style="background-image: url({{asset($event->image)}});"></a>
                                            <div class="text">
                                                <h3><a class="font-force" href="{{url('event/'.$event->id)}}"> لورم ایپسوم متن </a></h3>
                                                <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                                    استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در
                                                    ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                                                    کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                                <p><a href="{{url('event/'.$event->id)}}" class="btn btn-white btn-outline-white">اطلاعات بیشتر /
                                                        ثبت نام</a></p>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <div id="pagination" class="block-27">
                                            {{ $events[4]->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-5-tab">
                                <div class="row">
                                    @foreach($events[5] as $event)
                                        <div class="col-md-4 text-center">
                                            <div class="menu-wrap">
                                                <a href="{{url('event/'.$event->id)}}" class="menu-img img mb-4"
                                                   style="background-image: url({{asset($event->image)}});"></a>
                                                <div class="text">
                                                    <h3><a class="font-force" href="{{url('event/'.$event->id)}}"> لورم ایپسوم متن </a></h3>
                                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                                        استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در
                                                        ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                                                        کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                                    <p><a href="{{url('event/'.$event->id)}}" class="btn btn-white btn-outline-white">اطلاعات بیشتر /
                                                            ثبت نام</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <div id="pagination" class="block-27">
                                            {{ $events[5]->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="v-pills-7" role="tabpanel" aria-labelledby="v-pills-5-tab">
                                <div class="row">
                                    @foreach($events[6] as $event)
                                        <div class="col-md-4 text-center">
                                            <div class="menu-wrap">
                                                <a href="{{url('event/'.$event->id)}}" class="menu-img img mb-4"
                                                   style="background-image: url({{asset($event->image)}});"></a>
                                                <div class="text">
                                                    <h3><a class="font-force" href="{{url('event/'.$event->id)}}"> لورم ایپسوم متن </a></h3>
                                                    <p>لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با
                                                        استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در
                                                        ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و
                                                        کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
                                                    <p><a href="{{url('event/'.$event->id)}}" class="btn btn-white btn-outline-white">اطلاعات بیشتر /
                                                            ثبت نام</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="row mt-5">
                                    <div class="col text-center">
                                        <div id="pagination" class="block-27">
                                            {{ $events[6]->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@include('footer')
@include('script')
</body>
</html>