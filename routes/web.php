<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application    . These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'EventController@latest');

Route::get('/festival', 'EventController@by_type');
Route::get('/exhibition', 'EventController@by_type');
Route::get('/contest', 'EventController@by_type');
Route::get('/courses', 'EventController@by_type');
Route::get('/side_events', 'EventController@by_type');

//Route::get('/events', 'EventController@index');
Route::get('/event/{id}', 'EventController@show');
Route::post('/event/{id}', 'EnrollController@store');

Route::get('/gallery','MediaController@index');

Route::get('/video', function () {
    return view('videos');
});
Route::get('/invitation', function (){
    return view('invitation');
});
Route::get('/news', 'EventController@by_type');

Route::get('/about', function () {
    return view('about');
});

Route::get('/pishro', function () {
    return view('pishro');
});
Route::get('/bidarmaghz', function () {
    return view('bidarmaghz');
});
Route::get('/safaee', function () {
    return view('safaee');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/booth', function () {
    return view('booth');
});
Route::post('/booth','EnrollController@store');
Route::get('/mapSelection', function () {
    return view('mapSelection');
});
Route::get('/mapSelected','EnrollController@selectMap');
Route::get('/confirm','EnrollController@confirm');

Route::get('/mapInfo', function () {
    return view('mapInfo');
});


Route::get('admin/login', function (){
    return view('auth/login');
})->name('login');

Route::post('admin/login', 'Auth\LoginController@login');
Route::get('admin/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['prefix' => 'admin','middleware'=>'auth'], function () {
    Route::get('/', function () {
        return redirect('admin/registration');
    });

    Route::get('confirmEnroll','EnrollController@adminConfirm');
    Route::get('deleteEnroll','EnrollController@destroy');
    Route::post('updateEnroll','EnrollController@update');

    Route::get('events/new/{cat}', 'EventController@create')->name('new');
    Route::post('events/new/{cat}', 'EventController@store');

    Route::get('gallery/new/{cat}', 'MediaController@create');
    Route::post('gallery/new/{cat}', 'MediaController@store');
    Route::get('gallery/delete/{id}', 'MediaController@destroy');

    Route::get('sms', 'smsController@show');
    Route::post('sms/send', 'smsController@send');

    Route::get('/view', function () {
        return view('dashboard/viewpost');
    });
    Route::get('/registration', 'EnrollController@index');
    Route::get('/events/',function (){
        return view('dashboard/events')->with('events',[]);
    });

    Route::get('/events/{cat}','EventController@by_cat');

    Route::get('/gallery/{cat}','MediaController@by_cat');
});

