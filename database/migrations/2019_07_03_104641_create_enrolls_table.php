<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('sub_type');
            $table->string('name');
            $table->string('manager_name')->nullable();
            $table->text('address')->nullable();

            $table->string('booth')->nullable();
            $table->string('payment_id')->nullable();

            $table->string('email')->nullable();
            $table->string('instagram')->nullable();
            $table->string('telegram')->nullable();
            $table->string('agent_name')->nullable();
            $table->string('agent_post')->nullable();
            $table->string('phone')->nullable();
            $table->string('activity_field')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrolls');
    }
}
